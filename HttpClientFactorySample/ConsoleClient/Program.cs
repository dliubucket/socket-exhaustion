﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace ConsoleClient {
    class Program {
        static private IHttpClientFactory _httpClientFactory;
        static private HttpClient _httpClient;

        static async Task Main (string[] args) {
            var serviceProvider = new ServiceCollection ()
                .AddLogging (builder => {
                    builder.AddFilter ((category, level) => true);
                    builder.AddConsole ();
                })
                // .AddHttpClient ()
                .AddHttpClient ()
                // .AddPolicyHandler (GetRetryPolicy ())
                .BuildServiceProvider ();
            _httpClientFactory = serviceProvider.GetService<IHttpClientFactory> ();
            _httpClient = _httpClientFactory.CreateClient ("SingletonHttpClient");

            #region "Sequential-SingleConnection"
            /*
            for (int i = 0; i < 1000; i++) {
                var httpClient = httpClientFactory.CreateClient ();
                var response = await httpClient.GetAsync ("https://localhost:5001/weatherforecast").Result.Content.ReadAsStringAsync ();
                Console.WriteLine (response);
                Console.WriteLine (Environment.NewLine);
            }
            */
            #endregion "Sequential-SingleConnection"

            #region "Parallel-MultiConnections"
            var postResponses = new List<string> ();
            // running 100x10 = 1000 API calls
            for (int i = 0; i < 100; i++) {
                // Reusing 10 connections for the entire session
                var tasks = new List<Task> { };
                for (int j = 0; j < 10; j++) {
                    async Task<string> func (int i, int j) {
                        //var httpClient = _httpClientFactory.CreateClient ();
                        // return await httpClient.GetAsync ("https://localhost:5001/weatherforecast");
                        var response = await _httpClient.GetAsync ("https://localhost:5001/weatherforecast");
                        return await response.Content.ReadAsStringAsync ();
                    }

                    tasks.Add (func (i, j));
                }
                await Task.WhenAll (tasks);
                foreach (Task<string> t in tasks) {
                    var postResponse = await t;
                    postResponses.Add (postResponse);
                }
            }
            // Process the results from 1000 API calls
            foreach (var postResponse in postResponses) {
                var result = JsonConvert.DeserializeObject<IEnumerable<WeatherForecast>> (postResponse);
                Console.WriteLine (result.FirstOrDefault ().ToString ());
            }

        }
        #endregion "Parallel-MultiConnections"
    }
}