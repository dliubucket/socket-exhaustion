using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApiService.Controllers {
    [ApiController]
    [Route ("[controller]")]
    public class ValuesController : Controller {

        private readonly IHttpClientFactory _httpClientFactory;
        public ValuesController (IHttpClientFactory httpClientFactory) {
            _httpClientFactory = httpClientFactory;
        }

        [HttpGet]
        public async Task<ActionResult> Get () {
            // the HttpClientFactory will create a new instance of HttpClient each time.
            // HttpClientFactory pools HttpClientHandler instances and manages their lifetime
            // Once created the HttpClientHandlers are pooled and held for reuse for around for 2 minutes by default.
            // This pooling feature helps reduce the risk of socket exhaustion
            var client = _httpClientFactory.CreateClient ();
            // var result = await client.GetStringAsync ("http://www.google.com");
            var result = await client.GetStringAsync ("https://mocktarget.apigee.net/json");
            return Ok (result);
        }
    }
}