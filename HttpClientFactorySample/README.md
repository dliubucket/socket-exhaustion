# Intro

# Project Setup

```
dotnet new sln -n HttpClientFactorySample -o HttpClientFactorySample
cd HttpClientFactorySample
dotnet new api -n WebApiService
dotnet sln ./HttpClientFactorySample.sln add ./WebApiService/WebApiService.csproj
dotnet add ./WebApiService/WebApiService.csproj package Microsoft.Extensions.DependencyInjection
dotnet add ./WebApiService/WebApiService.csproj package Microsoft.Extensions.Http
dotnet add WebApiService/WebApiService.csproj package Microsoft.Extensions.Logging
dotnet add WebApiService/WebApiService.csproj package Microsoft.Extensions.Logging.Console
```

# Test WebApiService

```
https://localhost:5001/weatherforecast
https://localhost:5001/value
```

# Console Client

```
dotnet new console -n ConsoleClient
dotnet sln ./HttpClientFactorySample.sln add ./ConsoleClient/ConsoleClient.csproj
dotnet add ./ConsoleClient/ConsoleClient.csproj package Microsoft.Extensions.DependencyInjection
dotnet add ./ConsoleClient/ConsoleClient.csproj package Microsoft.Extensions.Http     
dotnet add ./ConsoleClient/ConsoleClient.csproj package Microsoft.Extensions.Logging
dotnet add ./ConsoleClient/ConsoleClient.csproj package Microsoft.Extensions.Logging.Console
```

## Test Run Console Client

- Sequential Single Connection
- Parallel Multiple Connections

```
netstat -na | grep 5001
```

Running parallel connection for a few minutes, we should see port exhaustion and
`netstat -na` returns many `TIME_WAIT` connections.

## Test Run Reuse HttpClient

When reusing connections, we are seeing only a few active connections at all times:

- it took about 5min to run all 1000 API calls
- it used only 10 sockets.

```
netstat -na | grep 5001 | wc -l
```