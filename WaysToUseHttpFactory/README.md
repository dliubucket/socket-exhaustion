# Intro

[Use IHttpClientFactory to implement resilient HTTP requests](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/implement-resilient-applications/use-httpclientfactory-to-implement-resilient-http-requests)  

[3 ways to use HTTPClientFactory](https://www.talkingdotnet.com/3-ways-to-use-httpclientfactory-in-asp-net-core-2-1/)

# Using HttpClientFactory Directly

```
dotnet run -p UseHttpFactoryDirectly/UseHttpFactoryDirectly.csproj
```

```
curl -X GET https://localhost:5001/api/values
```

# Using Named Client

```
dotnet run -p UseNameClient/UseNameClient.csproj
```

```
curl -i -X GET https://localhost:5001/api/values
netstat -na | grep 35.227.194.212
```