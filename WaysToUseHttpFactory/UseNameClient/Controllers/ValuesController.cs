using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace UseHttpFactoryDirectly.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class ValuesController : Controller {
        private readonly IHttpClientFactory _httpClientFactory;

        public ValuesController (IHttpClientFactory httpClientFactory) {
            _httpClientFactory = httpClientFactory;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult> Get () {
            var client = _httpClientFactory.CreateClient ("mocked"); // Use Named Client
            string result = await client.GetStringAsync ("/json");
            return Ok (result);
        }

        // GET api/values/5
        [HttpGet ("{id}")]
        public ActionResult<string> Get (int id) {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post ([FromBody] string value) { }

        // PUT api/values/5
        [HttpPut ("{id}")]
        public void Put (int id, [FromBody] string value) { }

        // DELETE api/values/5
        [HttpDelete ("{id}")]
        public void Delete (int id) { }
    }
}