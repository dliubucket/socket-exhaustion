using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace UseTypedClient {

    public interface IMockedClient {
        Task<string> GetData ();
    }
    public class MockedClient : IMockedClient {
        public HttpClient Client { get; private set; }
        public MockedClient (HttpClient httpClient) {
            httpClient.BaseAddress = new Uri ("https://mocktarget.apigee.net/");
            httpClient.DefaultRequestHeaders.Add ("Accept", "application/json");
            httpClient.DefaultRequestHeaders.Add ("User-Agent", "WaysToUseHttpFactory-Sample");
            Client = httpClient;
        }

        public async Task<string> GetData () {
            return await Client.GetStringAsync ("/json");
        }
    }
}