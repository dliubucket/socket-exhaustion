using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UseTypedClient;

namespace UseTypedClient.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class ValuesController : Controller {
        private readonly IMockedClient _mockedClient;

        public ValuesController (IMockedClient mockedClient) {
            _mockedClient = mockedClient;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult> Get () {
            // string result = await _mockedClient.Client.GetStringAsync ("/json");
            string result = await _mockedClient.GetData ();
            return Ok (result);
        }

        // GET api/values/5
        [HttpGet ("{id}")]
        public ActionResult<string> Get (int id) {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post ([FromBody] string value) { }

        // PUT api/values/5
        [HttpPut ("{id}")]
        public void Put (int id, [FromBody] string value) { }

        // DELETE api/values/5
        [HttpDelete ("{id}")]
        public void Delete (int id) { }
    }
}